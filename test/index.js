'use strict';

const server = require('../server');

describe('Module: MG', () => {
  before(done => {
    // Starts the server.
    server
      .init()
      .then(() => {
        done();
      })
      .catch(err => {
        done(err);
      });
  });
  // MG tests.
  require('./mg');
});
