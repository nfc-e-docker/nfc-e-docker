'use strict';
/**
 * @description Checks SEFAZ status.
 * @returns {Boolean} Returns true if SEFAZ is available or false otherwise.
 */
const status = async _uf => {
  return [null, true];
};

module.exports = status;
