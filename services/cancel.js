/**
 * Invoice cancel service
 */

'use strict';

const { mongo } = require('../lib');

const cancel = (key, callback) => {
  mongo
    .remove(key)
    .then(data => {
      callback(null, data.result.n === 1 || false);
    })
    .catch(err => {
      callback(err);
    });
};

module.exports = cancel;
