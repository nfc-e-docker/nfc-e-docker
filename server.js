'use strict';

require('dotenv').config();
const grpc = require('grpc');
const path = require('path');
const protoLoader = require('@grpc/proto-loader');
const { mongo, timeout, certs, promiseHandler } = require('./lib');
const { cancelService, statusService, processService, documentService } = require('./services');

let err = null;
let data = null;

module.exports.init = async () => {
  // Opens mongo connection
  [err, data] = await promiseHandler(mongo.openConnection());

  if (err) throw new Error(err);

  // Proto loading
  const packageDefinition = await protoLoader.load(path.join(__dirname, 'protos', 'mg.proto'));
  const InvoiceService = grpc.loadPackageDefinition(packageDefinition).InvoiceService;

  const server = new grpc.Server();

  server.addService(InvoiceService.service, {
    cancel: (call, callback) => {
      cancelService(call.request.key, (err, res) => {
        if (err) return callback(err);
        callback(null, { status: res });
      });
    },
    process: (call, callback) => {
      const { invoice, key, crt } = call.request;
      processService(invoice, key, crt, (err, res) => {
        if (err) return callback(err);
        callback(null, { invoice: res });
      });
    },
    document: (call, callback) => {
      documentService(call.request, (err, res) => {
        if (err) return callback(err);
        callback(null, { pdf: res });
      });
    },
    status: async (call, callback) => {
      const { uf } = call.request;
      [err, data] = await statusService(uf);
      if (err) return callback(err);
      callback(null, { status: data });
    },
    echo: (call, callback) => {
      callback(null, { pong: 'pong' });
    }
  });

  server.bind(
    `${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`,
    grpc.ServerCredentials.createSsl(certs.rootCerts, certs.keyCertPairs, true)
  );

  server.start();

  if (process.env.TEST_TYPE === 'integrated') {
    // Registers at gateway.
    timeout.connectToGateway((err, _res) => {
      if (err) {
        // Gracefully shuts down the server. The server will stop receiving new calls, and any pending calls will complete.
        server.tryShutdown(() => {
          process.exit(1);
        });
      }
      // Checks gateway heartbeat.
      timeout.heartbeat();
    });
  }
};
