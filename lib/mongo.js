'use strict';

require('dotenv').config();
const mongo = require('mongodb').MongoClient;

let collection = null;
let db = null;
const openConnection = () => {
  return new Promise((resolve, reject) => {
    mongo
      .connect(process.env.MONGO_URL, { useNewUrlParser: true })
      .then(client => {
        db = client.db(process.env.MONGO_DB);
        collection = db.collection(process.env.MONGO_COLLECTION);
        resolve();
      })
      .catch(err => {
        reject(err);
      });
  });
};

const insert = doc => collection.insertOne(doc);

const find = key => collection.findOne({ key });

const remove = key => collection.deleteOne({ key });

module.exports = { openConnection, insert, find, remove };
