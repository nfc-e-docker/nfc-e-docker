'use strict';

const createClient = require('./client');

module.exports = (name, callback) => {
  createClient(process.env.GATEWAY_HOST, process.env.GATEWAY_PORT, 'RegistryService')
    .then(_client => {
      const client = _client;
      client.discover({ name }, (err, res) => {
        if (err) return callback(err);
        callback(null, res);
      });
    })
    .catch(err => {
      callback(err);
    });
};
